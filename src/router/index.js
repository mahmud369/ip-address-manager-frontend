import { createRouter, createWebHistory } from 'vue-router'

const routes = [

  { path: '/home', name: 'Home', component: () => import('@/views/Home.vue')},
  { path: '/audit', name: 'Audit', component: () => import('@/views/Audit.vue')},
  { path: '/login', name: 'Login', component: () => import('@/views/Login.vue')},

]

const router = createRouter({
  history: createWebHistory(process.env.BASE_URL),
  routes
})


// router.beforeEach((to, from, next) => {

    // console.log(router.currentRoute.value.path);
    // console.log(from.name + ' ===== ' + to.name);

    // //let currentPath = router.currentRoute.value.path;
    // let authToken = localStorage.getItem('authToken');
    // let isAuthenticated =false;
    // axios.get("http://127.0.0.1:8090/api/user", {headers: {'Authorization': `Bearer ${authToken}`}})
    //   .then((res)=>{
    //       if(res){
    //         isAuthenticated = true;
    //       }
    //   })
    //   .catch((err)=>{
    //     console.log(err);
    //   });

    // if(! (currentPath == '/' || currentPath == '/login'))
    // {
    //     if(isAuthenticated)
    //       router.push('home');
    //     else
    //       router.push('login');
    // } 
    // else 
    // {
    //   if(isAuthenticated)
    //     next();
    //   else
    //     router.push('login');
    // }

    // let isAuthenticated =false;
    // let authToken = localStorage.getItem('authToken');
    // axios.get("http://127.0.0.1:8090/api/user", {headers: {'Authorization': `Bearer ${authToken}`}})
    // .then((res)=>{
    //   console.log(res);
    //   if(res.data)
    //     isAuthenticated = true;      
    // })
    // .catch((err)=>{
    //   console.log(err);
    // });
    // //next();

    // if(isAuthenticated) {
    //   let currentPath = router.currentRoute.value.path;
    //   if(currentPath == '/'){
    //     next({ name: 'Home' })
    //   }
    // } else {
    //   next({ name: 'Login' })
    // }

    /* if(to.name == 'Login'){
      if(from.name != undefined){
        next({ name: from.name })
      } else {
        next({ name: 'Home' })
      }
    } else {
      next();
    } */

      // let isAuthenticated =true;
      // console.log('==>>>> '+isAuthenticated);

      // if (to.name !== 'Login' && !isAuthenticated) 
      //   next({ name: 'Login' })
      // else 
      //   router.push('home');

// })

export default router;