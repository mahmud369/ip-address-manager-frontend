import { createApp } from 'vue'
import App from './App.vue'
import router from './router'

// Axios Imports
import axios from 'axios'
import VueAxios from 'vue-axios'

const app = createApp(App)

// Api Url & Credentials
app.config.globalProperties.$apiUrl = 'http://127.0.0.1:8090/api';

// app.config.globalProperties.$authToken = '';
// app.config.globalProperties.$apiHeader = {};
// app.config.globalProperties.$authUser = {};
// app.config.globalProperties.$isAuthenticated = false;

app.use(router)
app.use(VueAxios, axios)
app.provide('axios', app.config.globalProperties.axios)  // provide 'axios'

// Axios assigning for global access
app.config.globalProperties.$axios = axios;
// Router assigning for global access
app.config.globalProperties.$router = router;

app.mount('#app')
